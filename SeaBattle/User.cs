﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeaBattle
{
    public class User : Player
    {
        public User() : base()
        {
            
        }
        public override void Turn(Player victim)
        {
            (int, int) position = GetPosition();
            bool isHit = victim.GetHit(position.Item1, position.Item2);
            if (isHit)
            {
                Console.Clear();
                
                PlayerField.Print(false);
                victim.PlayerField.Print(true);
                Turn(victim);
            }
        }
        protected override (int,int) GetPosition()
        {
            (int, int) result;
            while (true)
            {
                try
                {
                    Console.Write("Введите от (А-J) и (1-10) через пробел : ");
                    string position = Console.ReadLine();
                    string[] parsed = position.Split(' ');
                    int y = parsed[0].ToCharArray()[0] - 'A';
                    int x = int.Parse(parsed[1]) - 1;
                    if (PlayerField.InField(x, y))
                    {
                        result = (x, y);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Такой клетки не существует");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return result;
        }
        protected override void AddShip(int size)
        {
            Console.WriteLine($"Заполните корабль {size}");
            Ship result = new Ship();
            for(int i = 0; i < size; i++)
            {
                try
                {
                    (int, int) position = GetPosition();
                    Cell tempCell = new Cell(position.Item1, position.Item2);
                    if (result.Validate(tempCell))
                    {
                        result.Body.Add(tempCell);
                        if (Validate(result))
                        {
                            result.Body.Remove(tempCell);
                            Console.WriteLine("Часть корабля задевает другие корабли\nПовторите ввод");
                            i--;

                        }
                    }
                    else
                    {
                        Console.WriteLine("Клетка нарушает правила расстоновки");
                        i--;

                    }
                }catch(Exception ex)
                {
                    Console.WriteLine($"{ex.Message}");
                    i--;
                }
            }
            Console.Clear();
            Ships.Add(result);
            PlayerField.AddShips(this);
            PlayerField.Print(false);
        }
    }
}
