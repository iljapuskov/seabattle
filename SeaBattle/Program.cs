﻿using System;

namespace SeaBattle
{
    class Program
    {
        private static bool _isWon;
        static void Main(string[] args)
        {
            _isWon = false;
            AI ai = new AI();
            User user = new User();
            Console.Clear();
            while (!_isWon)
            {
                user.PlayerField.Print(false);
                ai.PlayerField.Print(true);
                user.Turn(ai);
                ai.Turn(user);
                Console.Clear();
                if (user.IsLose)
                {
                    Console.WriteLine("Победил компьютер ");
                    _isWon = true;
                }
                else if(ai.IsLose)
                {
                    Console.WriteLine("Вы победили "); 
                    _isWon = true;
                }
            }
            Console.ReadKey();
        }
    }
}
