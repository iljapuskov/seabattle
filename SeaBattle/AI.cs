﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeaBattle
{
    public class AI : Player
    {
        private static Random _random;
        public AI() : base()
        {

        }
        public override void Turn(Player victim)
        {

            (int, int) position = GetPosition();
            if(victim.PlayerField.BattleField[position.Item1, position.Item2] == 'ø' || victim.PlayerField.BattleField[position.Item1, position.Item2] == 'x')
            {
                Turn(victim);
            }
            else
            {
                bool isHit = victim.GetHit(position.Item1, position.Item2);
                if (isHit)
                {
                    
                    Turn(victim);
                }
            }
            
        }

        protected override void AddShip(int size)
        {
            Ship result = new Ship();
            _random = new Random();
            
            for (int i = 0, c = 0; i < size; i++, c++)
            {
                if (c >= 1000)
                {
                    result = new Ship();
                    i = 0;
                    c = 0;
                }
                int y = _random.Next(0, 10);
                int x = _random.Next(0, 10);
                Cell tempCell = new Cell(x, y);
                if (result.Validate(tempCell))
                {
                    result.Body.Add(tempCell);
                    if (Validate(result))
                    {
                        result.Body.Remove(tempCell);
                        i--;
                    }
                }
                else
                {
                    i--;
                }
                
            }
            Ships.Add(result);
            PlayerField.AddShips(this);
        }
    
        protected override (int, int) GetPosition()
        {
            (int, int) position = (_random.Next(0, 10), _random.Next(0, 10));

            return position;
        }
    }
}
