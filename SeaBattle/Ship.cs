﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeaBattle
{
    public class Cell
    {
        private int _x;
        private int _y;
        private bool _isAlive;
        
        public Cell(int x,int y)
        {
            _x = x;
            _y = y;
            _isAlive = true;
        }
        public int X
        {
            get => _x;
        }
        public int Y
        {
            get => _y;
        }
        public bool IsAlive
        {
            get => _isAlive;
            set => _isAlive = value;
        }
    }
    public class Ship
    {
        private List<Cell> _body = new List<Cell>();
        private bool _isDestroy;
        private int _hitCount;
        public Ship(int xBegin,int yBegin, int xEnd, int yEnd)
        {
            Create(xBegin, yBegin, xEnd, yEnd);
            HitCount = 0;
            _isDestroy = false;
        }
        public Ship()
        {
            HitCount = 0;
            _isDestroy = false;
        }
        private void Create(int xBegin, int yBegin, int xEnd, int yEnd)
        {
            for (int i = yBegin; i <= yEnd; i++)
            {
                for (int j = xBegin; j <= xEnd; j++)
                {
                    Body.Add(new Cell(j, i));
                }
            }
        }

        public bool Hit(int x, int y)
        {
            for (int i = 0; i < Body.Count; i++) 
            {
                if ((Body[i].X == x && Body[i].Y == y) && Body[i].IsAlive) 
                {
                    Body[i].IsAlive = false;
                    HitCount += 1;
                    return true;
                }
            }
            return false;
        }
        
        public void Update()
        {
            if(HitCount == Body.Count)
            {
                _isDestroy = true;
            }
        }
        private bool Dublicate(Cell item)
        {
            bool result = true;

            foreach(Cell segment in Body)
            {
                if(segment.X == item.X && segment.Y == item.Y)
                {
                    result = false;
                }
            }

            return result;
        }
        private bool InOneDirection(Cell item)
        {
            bool result = true;
            int dirX = 0;
            int dirY = 0;
            for(int i = 0; i < Body.Count; i++)
            {
                dirX = (int)Math.Abs(Body[i].X - item.X);
                dirY = (int)Math.Abs(Body[i].Y - item.Y);
                if(dirX > 0 && dirY > 0)
                {
                    result = false;
                }
            }
            
            return result;
        }
        private bool DistanceValidateion(Cell cell)
        {
            bool result = false;

            if(Body.Count == 0)
            {
                result = true;
            }
            else
            {
                foreach (Cell segment in Body)
                {
                    int distance = (int)Math.Sqrt(
                                Math.Pow(cell.X - segment.X, 2)
                                + Math.Pow(cell.Y - segment.Y, 2)
                                );
                    if (distance == 1)
                    {
                        result = true;
                    }
                }
            }
            
            return result;
        }
        public bool Validate(Cell item)
        {
            bool result = false;

            if (Dublicate(item) && DistanceValidateion(item) && InOneDirection(item))
            {
                result = true;
            }
                
            return result;
        }
        public List<Cell> Body
        {
            get => _body;
            set => _body = value;
        }
        public bool IsDestroy
        {
            get => _isDestroy;
            set => _isDestroy = value;
        }
        public int HitCount
        {
            get => _hitCount;
            set => _hitCount = value;
        }
    }
}
