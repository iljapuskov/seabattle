﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeaBattle
{
    public class Field
    {
        private char[,] _battleField;
        public Field()
        {
            _battleField = new char[10, 10];
            Generate();
        }
        private void Generate()
        {
            for(int y = 0; y < _battleField.GetLength(0); y++)
            {
                for (int x = 0; x < _battleField.GetLength(1); x++)
                {
                    _battleField[y, x] = ' ';
                }
            }
        }
        public void Print(bool isHidden)
        {
            Console.WriteLine("  ABCDEFGHIJ");
            for (int y = 0; y < _battleField.GetLength(0); y++)
            {
                Console.Write($"{y + 1,2}");
                for (int x = 0; x < _battleField.GetLength(1); x++)
                {
                    if(_battleField[y, x] == '█' && isHidden)
                    {
                        Console.Write(' ');
                    }
                    else if (_battleField[y, x] == 'x')
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"{_battleField[y, x]}");
                        Console.ResetColor();
                    }
                    else if (_battleField[y, x] == '█')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($"{_battleField[y, x]}");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write($"{_battleField[y, x]}");
                    }


                }
                Console.WriteLine();
            }
        }
        public void AddShips(Player player)
        {
            foreach(Ship ship in player.Ships)
            {
                foreach (Cell cell in ship.Body)
                {

                    _battleField[cell.X, cell.Y] = '█';
                }
            }
        }
        public char[,] BattleField
        {
            get => _battleField;
            set => _battleField = value;
        }
        public bool InField(int x, int y)
        {
            return _battleField.GetLength(0) > y && y >= 0 && _battleField.GetLength(1) > x && x >= 0; 
        }
    }
}
