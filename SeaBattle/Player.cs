﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeaBattle
{
    public abstract class Player
    {
        private List<Ship> _ships;
        private Field _field;
        private bool _isLose;
        public Player()
        {
            _isLose = false;
            _ships = new List<Ship>();
            _field = new Field();
            Generate();
        }
        public bool GetHit(int x, int y)
        {
            bool result = false;
            
            foreach(Ship ship in Ships)
            {
                result = ship.Hit(x, y);
                if (result)
                {
                    PlayerField.BattleField[x, y] = 'x';
                    ship.Update();
                    Update();
                    if (ship.IsDestroy)
                    {
                        Console.WriteLine("Корабль уничтожен");
                        foreach (Cell segment in ship.Body)
                        {
                            for (int i = segment.X - 1; i <= segment.X + 1; i++)
                            {
                                for (int j = segment.Y - 1; j <= segment.Y + 1; j++)
                                {
                                    if (PlayerField.InField(i, j))
                                    {
                                        if (PlayerField.BattleField[i, j] == ' ')
                                        {
                                            PlayerField.BattleField[i, j] = 'ø';
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    break;
                    
                }
                else
                {
                    PlayerField.BattleField[x, y] = 'ø';
                }
            }

            return result;
        }
        public abstract void Turn(Player victim);
 
        protected bool Validate(Ship item)
        {
            bool result = false;

            foreach(Ship ship in Ships)
            {
                foreach(Cell segment in item.Body)
                {
                    foreach(Cell cell in ship.Body)
                    {
                        int distance = (int)Math.Sqrt(
                            Math.Pow(cell.X - segment.X, 2)
                            + Math.Pow(cell.Y - segment.Y, 2)
                            );
                        if(distance<2)
                        {
                            result = true;
                            return result;
                        }
                    }
                }
            }

            return result;
        }
        protected void Generate()
        {
            for (int i = 4; i > 0; i--)
            {
                for (int j = 0; j < i; j++)
                {

                    AddShip(5 - i);
                }
            }
        }
        protected void Update()
        {
            bool flag = false;
            foreach(Ship ship in Ships)
            {
                if (!ship.IsDestroy)
                {
                    flag = true;
                }
            }
            if (!flag)
            {
                IsLose = true; 
            }
        }
        protected abstract (int, int) GetPosition();
        protected abstract void AddShip(int size);
        public List<Ship> Ships
        {
            get => _ships;
            set => _ships = value;
        }
        public Field PlayerField
        {
            get => _field;
            set => _field = value;
        }
        public bool IsLose
        {
            get => _isLose;
            set => _isLose = value;
        }
    }
}
